﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuFuncMain : MenuFunctionality
{
    public GameObject IPAddressField;

    public void Connect()
    {
        //Turn on the IP Address Input field
        IPAddressField.SetActive(true);
        //Debug.Log("Connect to server or whatever Nick wants this function to do...");
    }

    public void QuitGame()
    {
        Application.Quit();

        // Debug.Log here for in-editor testing
        Debug.Log("Calling Application.Quit()...");
    }
    public void LoadClient(string serverAddress)
    {
        string test = serverAddress;
        Debug.Log(test);
    }

    public void ErrorMessage()
    {

    }
}
