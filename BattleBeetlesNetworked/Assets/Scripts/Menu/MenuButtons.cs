﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButtons : MonoBehaviour
{
    [Header("List of Buttons")]
    public MenuButton[] buttons;

    [Header("Menu specific paramters")]
    public bool invertUpDown;
    public int curr = 0;
    public float menuCooldown;
    private bool canMove = true;

    private MenuFunctionality refMenuFunc;

    void Start()
    {
        refMenuFunc = GetComponent<MenuFunctionality>();

        // log a warning in case the menu functionality script couldn't be found
        if (!refMenuFunc)
        {
            Debug.LogWarning("Matt says... the MenuFunctionality script could not be found, so this menu probably doesn't do anything...");
        }
    }

    void Update()
    {
        DoVisuals();
        GetInput();
    }

    private void DoVisuals()
    {
        for (int i = 0; i < buttons.Length; ++i)
        {
            if (i == curr)
            {
                // do visuals for selected option
                buttons[i].obj.transform.localScale = Vector3.Lerp(buttons[i].obj.transform.localScale, new Vector3(1.2f, 1.2f, 1.2f), 0.5f);
            }
            else
            {
                // reset visuals
                buttons[i].obj.transform.localScale = Vector3.Lerp(buttons[i].obj.transform.localScale, new Vector3(1.0f, 1.0f, 1.0f), 0.5f);
            }
        }
    }

    private void GetInput()
    {
        // navigating the menu
        if (Input.GetAxis("VerticalLeft") > 0.1f && canMove)
        {
            ChangeCurrent(true);
            DisableMenu();
        }
        if (Input.GetAxis("VerticalLeft") < -0.1f && canMove)
        {
            ChangeCurrent(false);
            DisableMenu();
        }

        // making a selection
        if (Input.GetButtonDown("Attack"))
        {
            SendMessage(buttons[curr].method);
        }
    }

    private void ChangeCurrent(bool up)
    {
        // user pressed up
        if (up)
        {
            // change value of curr based on the inversion setting
            if (invertUpDown)
            {
                DecrementCurrent();
            }
            else
            {
                IncrementCurrent();
            }
        }
        // user pressed down
        else
        {
            // change value of curr based on the inversion setting
            if (invertUpDown)
            {
                IncrementCurrent();
            }
            else
            {
                DecrementCurrent();
            }
        }
    }

    private void DecrementCurrent()
    {
        // decrease the current value, or set it to the max
        if (curr > 0)
        {
            curr--;
        }
        else
        {
            curr = buttons.Length - 1;
        }
    }

    private void IncrementCurrent()
    {
        // increment the current value, or set it to 0
        if (curr < buttons.Length - 1)
        {
            curr++;
        }
        else
        {
            curr = 0;
        }
    }

    private void DisableMenu()
    {
        // disable menu for a short time
        canMove = false;
        Invoke("ReenableMenu", menuCooldown);
    }

    private void ReenableMenu()
    {
        // reenable menu
        canMove = true;
    }
}

[System.Serializable]
public struct MenuButton
{
    public GameObject obj;
    public string method;
};