﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// need this for plugins
using System.Runtime.InteropServices;

public class UtilityUserManager : MonoBehaviour
{
    [DllImport("egp-net-plugin-Unity")]
    static extern PlayerData getStruct();

    [Header("Player Controlled Prefab")]
    public GameObject playerPrefab;
    [Header("Dummy Prefab")]
    public GameObject dummyPlayer;
    [HideInInspector]
    public int numOfPlayers;

    [Header("Spawns")]
    public GameObject[] spawnTransforms;

    public int clientNum;
    public int numToSpawn;
    int nextID;

    [SerializeField]
    List<PlayerStatus> playerList;


    private void Start()
    {
        playerList = new List<PlayerStatus>();
        numOfPlayers = 0;
        nextID = 1;
        SpawnPlayer();
        
        // spawn dummy player on start for testing
        //numOfPlayers = 1;
        for(int i = 0; i < numToSpawn; i++)
        {
            SpawnDummy();
        }
        
    }

    public void SpawnDummy()
    {

        // increase number of players
        ++numOfPlayers;
        
        // spawn dummy player
        GameObject tmp = Instantiate(dummyPlayer, spawnTransforms[numOfPlayers - 1].transform.position, spawnTransforms[numOfPlayers - 1].transform.rotation);
        Debug.Log(spawnTransforms[numOfPlayers - 1].transform.position);

        // set values for dummy player
        if (nextID == clientNum)
            nextID++;

        tmp.GetComponent<PlayerStatus>().playerNum = nextID;
        tmp.GetComponent<PlayerNetworking>().isDummy = true;

        nextID++;
        
        
        playerList.Add(tmp.GetComponent<PlayerStatus>());
        
    }

    public void SpawnPlayer()
    {
        // increase number of players
        ++numOfPlayers;

        // spawn dummy player
        GameObject tmp = Instantiate(playerPrefab, spawnTransforms[numOfPlayers - 1].transform.position, spawnTransforms[numOfPlayers - 1].transform.rotation);

        // set values for dummy player
        tmp.GetComponent<PlayerStatus>().playerNum = clientNum;
        tmp.GetComponent<PlayerNetworking>().isDummy = false;


        playerList.Add(tmp.GetComponent<PlayerStatus>());
    }

    public void UpdatePlayer(PlayerData data)
    {
        for(int i = 0; i < playerList.Count; i++)
        {
            if(data.playerNum == playerList[i].playerNum)
            {
                playerList[i].gameObject.SendMessage("ReceiveData", data);
            }
        }
    }
}

[System.Serializable]
public struct SpawnPoint
{
    public Vector3 position;
    public Vector3 rotation;
}