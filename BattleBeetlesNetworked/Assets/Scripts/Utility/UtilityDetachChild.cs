﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UtilityDetachChild : MonoBehaviour
{
    void Start()
    {
        transform.SetParent(null);
    }
}
