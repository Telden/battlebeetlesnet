﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
// need this for plugins
using System.Runtime.InteropServices;

[System.Serializable]
public struct PlayerData
{
    //Player number
    public int playerNum;
    public int attackState;

    //Position and rotation for main gameplay
    public float posX, posY, posZ;
    public float rotX, rotY, rotZ;

    //Velocity and acceleration for dead reckoning and correction
    public float velX, velY, velZ;
    public float accelX, accelY, accelZ;
};

[System.Serializable]
public struct StateData
{
    public int state;
    public int active;
};

[System.Serializable]
public struct NetworkStatus
{
    public int newUpdates;
    public int newConnection;
    public int youWin;
};

[System.Serializable]
public struct ConnectionData
{
    public int num;
    public int totalPlayers;
};

public class unityRakNet : MonoBehaviour
{
    [DllImport("egp-net-plugin-Unity")]
    static extern int NetworkingUpdate();

    [DllImport("egp-net-plugin-Unity")]
    static extern int NetworkingStop();

    [DllImport("egp-net-plugin-Unity")]
    static extern int NetworkingSet(int test);

    [DllImport("egp-net-plugin-Unity")]
    static extern int NetworkingGet();

    [DllImport("egp-net-plugin-Unity")]
    static extern PlayerData getStruct();

    [DllImport("egp-net-plugin-Unity")]
    static extern int ClientInit(string ipAddress);

    [DllImport("egp-net-plugin-Unity")]
    static extern int ServerInit();

    [DllImport("egp-net-plugin-Unity")]
    static extern int sendStruct(PlayerData data);

    [DllImport("egp-net-plugin-Unity")]
    static extern NetworkStatus GetStatus();

    [DllImport("egp-net-plugin-Unity")]
    static extern ConnectionData GetConnectionData();

    [DllImport("egp-net-plugin-Unity")]
    static extern int ResetVariables();

    [DllImport("egp-net-plugin-Unity")]
    static extern int ILose(int losingID);

    [DllImport("egp-net-plugin-Unity")]
    static extern int CheckIfWon();

    public bool shouldNetwork;

    bool isInitialized = false;

    //Networking Structures
    NetworkStatus netStatus;
    PlayerData currentState;
    ConnectionData connectData;

    public GameObject Player;

    public bool shouldSend;

    UtilityUserManager refPlayerManager;

    void Start ()
    {
        //Get required references
        refPlayerManager = GetComponent<UtilityUserManager>();
       
        //Set up client
        String ipAddress = "127.0.0.1";
        Debug.Log(ipAddress);
       
        //Get the player num
        ClientInit(ipAddress);int playerNum = 0;
        if(shouldNetwork)
        {
            while (playerNum == 0)
            {
                NetworkingUpdate();
                connectData = GetConnectionData();
                playerNum = connectData.num;
            }
            refPlayerManager.clientNum = playerNum;
            refPlayerManager.numToSpawn = connectData.totalPlayers - 1; //Minus 1 to not count self
       
            isInitialized = true;
        }
	}
	
	void Update ()
    {
        if(isInitialized)
        {
            if (shouldSend)
            {
                SendPosition();
            }
            NetworkingUpdate();
            UpdatePosition();
            CheckStatus();
        }
	}


    void SendPosition()
    {
        sendStruct(currentState);
    }

    void UpdatePosition()
    {
        currentState = getStruct();
        refPlayerManager.UpdatePlayer(currentState);
    }

    public void SendPlayerData(PlayerData data)
    {
        currentState = data;
    }

    void CheckStatus()
    {
        netStatus = GetStatus();

        if (netStatus.newUpdates == 1)
        {
            if (netStatus.newConnection == 1)
            {
                refPlayerManager.SpawnDummy();
            }
            if(netStatus.youWin == 1)
            {
                Debug.Log("You win!");
            }

        }
    }

    void OnApplicationQuit()
    {
        Debug.Log("Reseting Variables");
        ResetVariables();
    }

    public void TellLose(int ID)
    {
        Debug.Log("TellLose");
        ILose(ID);
    }

    public bool CheckWin()
    {
        Debug.Log("Win state is " + CheckIfWon());
        return (CheckIfWon() == 1);
    }

    public void clientConnect(string ipAddress)
    {
        String ip = ipAddress;

        Debug.Log(ip);

        ClientInit(ip);
        //Get the player num
        int playerNum = 0;
        if (shouldNetwork)
        {
            while (playerNum == 0)
            {
                NetworkingUpdate();
                connectData = GetConnectionData();
                playerNum = connectData.num;
            }
        }
        Debug.Log("Connection established");
    }



}

public struct VectorMessage
{
    int message_id;
    float x, y, z;
};