﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    [Header("UI References")]
    public Slider healthBar;

    [Header("Win/Lose Text")]
    public Text youWin;
    public Text youLose;

    private PlayerStatus status;

    void Start()
    {
        status = GetComponent<PlayerStatus>();
    }

    void Update()
    {
        UpdateHealth();

        UpdateYouWinLose();
    }

    private void UpdateHealth()
    {
        healthBar.value = (float)status.refCollision.currHealth / status.refCollision.maxHealth;
    }

    private void UpdateYouWinLose()
    {
        if (status.refCollision.currHealth <= 0)
        {
            youLose.gameObject.SetActive(true);
        }

        if (status.refUnityRakNet.CheckWin())
        {
            youWin.gameObject.SetActive(true);
        }
    }
}
