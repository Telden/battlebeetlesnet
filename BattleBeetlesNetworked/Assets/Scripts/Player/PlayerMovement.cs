﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("Movement Parameters")]
    public float spd;
    public float spdLess;
    public bool canMove = true;

    [Header("Rotation Parameters")]
    public float rot;
    public float rotLess;

    [Header("Jump Parameters")]
    public float jumpForce;
    public bool inControl = true;

    [Header("Ground")]
    public bool isGrounded;
    public Transform groundTransform;
    public LayerMask groundMask;

    private PlayerStatus status;

    void Start()
    {
        // find our reference objects
        status = GetComponent<PlayerStatus>();
    }

    void FixedUpdate()
    {
        Movement();
        Rotation();
    }

    void Update()
    {
        CheckGround();
        Jumping();

        PseudoKillPlane();
    }

    private void Movement()
    {
        if (canMove)
        {
            // get vectors relative to the camera
            Vector3 cameraForward = status.refCamera.transform.forward.normalized;
            Vector3 cameraRight = status.refCamera.transform.right.normalized;

            // get input axes
            float axisHor = Input.GetAxis("HorizontalLeft");
            float axisVer = Input.GetAxis("VerticalLeft");

            float tmp = inControl ? spd : spdLess;

            // set the velocity
            // math found here: https://answers.unity.com/questions/46845/face-forward-direction-of-movement.html
            Vector3 movementVec = ((cameraForward * axisVer) + (cameraRight * axisHor)).normalized * tmp;
            movementVec = new Vector3(movementVec.x, status.rb.velocity.y, movementVec.z);

            if (inControl)
            {
                status.rb.velocity = movementVec;
            }
            else
            {
                status.rb.AddForce(movementVec);
            }
        }
    }

    private void Rotation()
    {
        if (canMove)
        {
            // get input axes
            float axisHor = Input.GetAxis("HorizontalRight");
            float axisVer = Input.GetAxis("VerticalRight");

            transform.Rotate(0.0f, axisHor * Time.deltaTime * rot, 0.0f);
        }
    }

    private void Jumping()
    {
        if (canMove)
        {
            if (Input.GetButtonDown("Jump") && isGrounded == true)
            {
                // reset y velocity back to zero just in case
                status.rb.velocity = new Vector3(status.rb.velocity.x, 0.0f, status.rb.velocity.z);

                // add jump force
                status.rb.AddForce(Vector3.up * jumpForce);

                // play the jump sound
                status.refAudio.PlayJump();
            }
        }
    }

    private void CheckGround()
    {
        // check for ground at the position of groundTransform
        isGrounded = Physics.OverlapSphere(groundTransform.position, 0.2f, groundMask).LongLength > 0;

        // stop any knocback
        if (isGrounded && status.refCollision.knockedBack)
        {
            status.refCollision.knockedBack = false;
            canMove = true;
        }

        inControl = isGrounded || status.refHover.isHovering;
    }

    public void DisableMovement(float duration)
    {
        canMove = false;
        Invoke("EnableMovement", duration);
    }

    public void EnableMovement()
    {
        canMove = true;
    }

    private void PseudoKillPlane()
    {
        if (transform.position.y < -10)
        {
            transform.position = new Vector3(0.0f, 1.0f, 0.0f);
        }
    }
}