﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityHover : MonoBehaviour
{
    [Header("Hover Parameters")]
    public float hoverDuration;
    public bool isHovering;
    private bool canHover;

    private PlayerStatus status;

    private void Start()
    {
        status = GetComponent<PlayerStatus>();
    }

    private void Update()
    {
        if (!status.refNetworking.isDummy)
        {
            GetInput();
            Hover();
        }
    }

    private void GetInput()
    {
        if (Input.GetButtonDown("Jump") && !status.refMovement.isGrounded && !isHovering && canHover && !status.refGroundPound.isPounding)
        {
            isHovering = true;
            canHover = false;
            Invoke("StopHovering", hoverDuration);
        }
    }

    private void Hover()
    {
        if (isHovering)
        {
            status.rb.velocity = new Vector3(status.rb.velocity.x, 0.0f, status.rb.velocity.z);
        }

        if (status.refMovement.isGrounded)
        {
            canHover = true;
        }
    }

    public void StopHovering()
    {
        isHovering = false;
        CancelInvoke("StopHovering");
    }
}
