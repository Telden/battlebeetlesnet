﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityLunge : MonoBehaviour
{
    [Header("Lunging Parameters")]
    public float lungeSpd;
    public float lungeDuration;
    public float cooldown;
    public GameObject hitbox;
    public bool isLunging;
    private bool canLunge = true;

    private PlayerStatus status;

    private void Start()
    {
        status = GetComponent<PlayerStatus>();

        status.ActivateHitbox(hitbox, false);
    }

    private void Update()
    {
        if (!status.refNetworking.isDummy)
        {
            GetInput();
            Lunge();
        }
    }

    private void GetInput()
    {
        // mark that we're lunging
        if (Input.GetButtonDown("Attack") && !isLunging && canLunge && status.refMovement.canMove)
        {
            isLunging = true;
            canLunge = false;

            // play a sound
            status.refAudio.PlayDash();

            // tell networking we've started lunging
            status.SendNetPlayerState((int)AttackState.STATE_LUNGE_ATTACK, 1);

            Invoke("StopLunge", lungeDuration);
        }
    }

    private void Lunge()
    {
        if (isLunging)
        {
            // activate hitbox and disable movement
            status.ActivateHitbox(hitbox, true);
            status.refMovement.canMove = false;

            // set velocity
            Vector3 tmp = transform.forward * lungeSpd;
            tmp.y = status.rb.velocity.y;
            status.rb.velocity = tmp;
        }
        else
        {
            // disable hitbox
            status.ActivateHitbox(hitbox, false);
        }
    }

    public void StopLunge()
    {
        // reset values to stop lunging
        isLunging = false;
        status.refMovement.canMove = true;
        Invoke("RechargeLunge", cooldown);

        // tell networking that we've stopped lunging
        status.SendNetPlayerState((int)AttackState.STATE_LUNGE_ATTACK, 0);
    }

    private void RechargeLunge()
    {
        canLunge = true;
    }
}