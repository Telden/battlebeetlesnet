﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityGroundPound : MonoBehaviour
{
    [Header("Ground Pounding Parameters")]
    public float poundSpd;
    public float cooldown;
    public GameObject hitbox;
    public bool isPounding;
    private bool canPound = true;

    private PlayerStatus status;

    private void Start()
    {
        status = GetComponent<PlayerStatus>();

        status.ActivateHitbox(hitbox, false);
    }

    private void Update()
    {
        if (!status.refNetworking.isDummy)
        {
            GetInput();
            GroundPound();
        }
    }

    private void GetInput()
    {
        if (Input.GetButtonDown("Attack3") && !status.refMovement.isGrounded && canPound)
        {
            // mark that we're ground pounding
            isPounding = true;
            status.ActivateHitbox(hitbox, true);

            // stop hovering
            status.refHover.SendMessage("StopHovering");

            // stop lunging
            status.refLunge.SendMessage("StopLunge");

            // stop dashing
            status.refDash.SendMessage("StopDash");

            // tell networking we've started ground pounding
            status.SendNetPlayerState((int)AttackState.STATE_GROUNDPOUND_ATTACK, 1);
        }
    }

    private void GroundPound()
    {
        // if we're ground pounding
        if (isPounding)
        {
            // if we're in the air
            if (!status.refMovement.isGrounded)
            {
                // fall downwards
                status.rb.velocity = new Vector3(0.0f, poundSpd * -1, 0.0f);
            }
            else
            {
                // otherwise stop ground pounding and disable movement for a short time
                isPounding = false;
                canPound = false;
                status.ActivateHitbox(hitbox, false);
                status.refMovement.canMove = false;
                status.rb.velocity = new Vector3(0.0f, status.rb.velocity.y, 0.0f);
                Invoke("RechargePound", cooldown);

                // tell networking that we've stopped ground pounding
                status.SendNetPlayerState((int)AttackState.STATE_GROUNDPOUND_ATTACK, 0);
            }
        }
        else
        {
            // disable hitbox
            status.ActivateHitbox(hitbox, false);
        }
    }

    private void RechargePound()
    {
        canPound = true;
        status.refMovement.canMove = true;
    }
}
