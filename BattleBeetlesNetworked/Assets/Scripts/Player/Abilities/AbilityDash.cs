﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityDash : MonoBehaviour
{
    [Header("Dashing Parameters")]
    public float dashSpd;
    public float dashDuration;
    public float cooldown;
    public bool isDashing;
    private bool canDash = true;

    private PlayerStatus status;

    private void Start()
    {
        status = GetComponent<PlayerStatus>();
    }

    private void Update()
    {
        if (!status.refNetworking.isDummy)
        {
            GetInput();
            Dash();
        }
    }

    private void GetInput()
    {
        if (Input.GetButtonDown("Attack2") && !isDashing && canDash && status.refMovement.canMove && CheckVelocity())
        {
            // mark that we're dashing
            isDashing = true;
            canDash = false;
            Invoke("StopDash", dashDuration);
        }
    }

    private void Dash()
    {
        // if we're dashing
        if (isDashing)
        {
            // stop movement
            status.refMovement.canMove = false;

            // get vectors relative to the camera
            Vector3 cameraForward = status.refCamera.transform.forward.normalized;
            Vector3 cameraRight = status.refCamera.transform.right.normalized;

            // get input axes
            float axisHor = Input.GetAxis("HorizontalLeft");
            float axisVer = Input.GetAxis("VerticalLeft");

            Vector3 movementVec = ((cameraForward * axisVer) + (cameraRight * axisHor)).normalized * dashSpd;
            movementVec = new Vector3(movementVec.x, status.rb.velocity.y, movementVec.z);

            // set velocity
            movementVec.y = 0.0f;
            status.rb.velocity = movementVec;
        }
    }

    public void StopDash()
    {
        // disable dashing
        isDashing = false;
        status.refMovement.canMove = true;
        Invoke("RechargeDash", cooldown);
    }

    private void RechargeDash()
    {
        canDash = true;
    }

    private bool CheckVelocity()
    {
        // check if we're moving in the x or z directions at all
        return (Mathf.Abs(status.rb.velocity.x) > 0.1f || Mathf.Abs(status.rb.velocity.z) > 0.1f);
    }
}
