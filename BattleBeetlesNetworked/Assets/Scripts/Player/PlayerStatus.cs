﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatus : MonoBehaviour
{
    [Header("Player Info")]
    public int playerNum;
    public int attackState;
    public Camera refCamera;

    // player references
    [HideInInspector]
    public PlayerMovement refMovement;
    [HideInInspector]
    public PlayerNetworking refNetworking;
    [HideInInspector]
    public PlayerAudio refAudio;
    [HideInInspector]
    public PlayerCollision refCollision;
    [HideInInspector]
    public PlayerUI refUI;

    // player abilities
    [HideInInspector]
    public AbilityDash refDash;
    [HideInInspector]
    public AbilityLunge refLunge;
    [HideInInspector]
    public AbilityHover refHover;
    [HideInInspector]
    public AbilityGroundPound refGroundPound;

    // other references
    [HideInInspector]
    public Rigidbody rb;
    [HideInInspector]
    public CameraFollow refCameraFollow;
    [HideInInspector]
    public unityRakNet refUnityRakNet;

    private void Awake()
    {
        // player scripts
        refMovement = GetComponent<PlayerMovement>();
        refNetworking = GetComponent<PlayerNetworking>();
        refAudio = GetComponent<PlayerAudio>();
        refCollision = GetComponent<PlayerCollision>();

        // abilities
        refDash = GetComponent<AbilityDash>();
        refLunge = GetComponent<AbilityLunge>();
        refHover = GetComponent<AbilityHover>();
        refGroundPound = GetComponent<AbilityGroundPound>();

        // rigidbody
        rb = GetComponent<Rigidbody>();

        // camera
        refCameraFollow = FindObjectOfType<CameraFollow>();

        // networking plugin
        refUnityRakNet = FindObjectOfType<unityRakNet>();
    }

    public void SendNetPlayerState(int state, int active)
    {
        // tell networking that we're lunging
        StateData data;
        data.state = state;
        data.active = active;
        
        // based on if the ability is active and what ability it is, update the attack state
        // ability is active
        if (data.active == 1)
        {
            switch (data.state)
            {
                case (int)AttackState.STATE_LUNGE_ATTACK:
                {
                    // call plugin function to say that the player is lunging
                    attackState = 1;
                    break;
                }
                case (int)AttackState.STATE_GROUNDPOUND_ATTACK:
                {
                    // call plugin function to say that the player is ground pounding
                    attackState = 2;
                    break;
                }
                default:
                {
                    break;
                }
            }
        }
        // ability is inactive
        else
        {
            switch (data.state)
            {
                case (int)AttackState.STATE_LUNGE_ATTACK:
                {
                    // call plugin function to say that the player has stopped lunging
                    attackState = 0;
                    break;
                }
                case (int)AttackState.STATE_GROUNDPOUND_ATTACK:
                {
                    // call plugin function to say that the player has stopped ground pounding
                    attackState = 0;
                    break;
                }
                default:
                {
                    break;
                }
            }
        }
    }

    public void ActivateHitbox(GameObject obj, bool activate)
    {
        // for both the player and dummy players
        // general function for activating hitboxes for any ability that needs it
        obj.SetActive(activate);
    }
}
