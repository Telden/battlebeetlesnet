﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    [Header("Health")]
    public int maxHealth;
    //[HideInInspector]
    public int currHealth;

    [Header("Invulnerability")]
    public float invulDuration;
    public bool invulnerable;

    [Header("Damage")]
    public int damageLunge;

    [Header("Knockback")]
    public bool applyAway;
    public Vector2 kbForce;
    public bool knockedBack;

    private PlayerStatus status;

    void Start()
    {
        // find our reference objects
        status = GetComponent<PlayerStatus>();

        currHealth = maxHealth;
    }

    private void Update()
    {
        CheckHealth();
    }

    private void CheckHealth()
    {
        if (!status.refNetworking.isDummy && currHealth <= 0)
        {
            Debug.Log("CheckHealth: " + currHealth);
            // if health is less than zero, we lost
            // notify player networking so the plugin can tell the server
            status.refNetworking.OutOfHealth();

            // stop movement
            status.refMovement.canMove = false;
            status.rb.velocity = Vector3.zero;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // collision with attack hitboxes
        if (other.CompareTag("Attack") && invulnerable == false)
        {
            // decrease health
            currHealth -= damageLunge;

            ApplyKnockback(other.transform.position);

            // activate invulnerability
            invulnerable = true;
            Invoke("MakeVulnerable", invulDuration);

            // play hit sound
            status.refAudio.PlayHit();
        }
    }

    private void MakeVulnerable()
    {
        // deactivate invulnerability
        invulnerable = false;
    }

    private void ApplyKnockback(Vector3 away)
    {
        // stop other things
        status.refHover.isHovering = false;
        status.refGroundPound.isPounding = false;
        status.refDash.isDashing = false;
        status.refLunge.isLunging = false;

        if (!knockedBack)
        {
            // stop moving
            status.refMovement.canMove = false;
            status.rb.velocity = Vector3.zero;

            // apply knockback
            Vector3 knockback;

            if (applyAway)
            {
                knockback = (away - transform.position).normalized * -1 * kbForce.x;
            }
            else
            {
                knockback = transform.forward * -1 * kbForce.x;
            }

            knockback.y = kbForce.y;

            status.rb.AddForce(knockback);
        }

        knockedBack = true;
    }
}
