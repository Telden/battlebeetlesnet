﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudio : MonoBehaviour
{
    [Header("Sounds")]
    public PlayerSound jump;
    public PlayerSound hit;
    public PlayerSound dash;

    private UtilityAudioManager refAudioManager;

    private void Start()
    {
        refAudioManager = FindObjectOfType<UtilityAudioManager>();
    }

    public void PlayJump()
    {
        refAudioManager.PlaySound(jump.clip, jump.volume, true);
    }

    public void PlayHit()
    {
        refAudioManager.PlaySound(hit.clip, hit.volume, true);
    }

    public void PlayDash()
    {
        refAudioManager.PlaySound(dash.clip, dash.volume, true);
    }
}

[System.Serializable]
public struct PlayerSound
{
    public AudioClip clip;
    public float volume;
}