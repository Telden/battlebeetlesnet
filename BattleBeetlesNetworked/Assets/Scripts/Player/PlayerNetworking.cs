﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNetworking : MonoBehaviour
{
    [Header("Sending data")]
    public float customDelay;
    public bool isDummy = false;

    [Header("Vectors for Dead Reckoning")]
    public Vector3 savedPosition;
    public Vector3 savedRotation;
    public Vector3 savedVelocity;
    public Vector3 savedAcceleration;

    private Vector3 prevVelocity; // stores previous velocity for calculating acceleration
    private float prevTime; // stores the previous time for calculating acceleration

    private PlayerStatus status;
    
    private void Start()
    {
        status = GetComponent<PlayerStatus>();

        // if we're not a dummy player, start sending our data to the server
        if (!isDummy)
        {
            // start sending data
            StartCoroutine("CustomFixedUpdate");
        }
    }

    // functions to handle sending player related information to the networking plugin
    public void ReceiveData(PlayerData data)
    {
        if(isDummy)
        {
            Debug.Log("Recieved Player Data");

            Vector3 position = new Vector3(data.posX, data.posY, data.posZ);
            Vector3 rotation = new Vector3(data.rotX, data.rotY, data.rotZ);
            Vector3 velocity = new Vector3(data.velX, data.velY, data.velZ);
            Vector3 acceleration = new Vector3(data.accelX, data.accelY, data.accelZ);

            savedPosition = position;
            savedRotation = rotation;
            savedVelocity = velocity;
            savedAcceleration = acceleration;

            switch(data.attackState)
            {
                case 0:
                {
                    // stop attacking
                    status.ActivateHitbox(status.refLunge.hitbox, false);
                    status.ActivateHitbox(status.refGroundPound.hitbox, false);
                    break;
                }
                case 1:
                {
                    // do lunge
                    status.ActivateHitbox(status.refLunge.hitbox, true);
                    break;
                }
                case 2:
                {
                    // do ground pound
                    status.ActivateHitbox(status.refGroundPound.hitbox, true);
                    break;
                }
            }
        }
    }

    private void Update()
    {
        // if we're a dummy player, simulate position, rotation, etc using dead reckoning
        if (isDummy)
        {
            Simulate();
        }
    }

    private void Simulate()
    {
        // set position using dead reckoning
        transform.position = DeadReckon(savedPosition, savedVelocity, savedAcceleration);

        // update positions
        savedPosition = transform.position;
        savedVelocity = status.rb.velocity;
        savedAcceleration = (status.rb.velocity - prevVelocity) / Time.deltaTime;

        // set rotation (could theoretically do dead reckoning using angular velocity and acceleration)
        transform.rotation = Quaternion.Euler(savedRotation);
    }

    private IEnumerator CustomFixedUpdate()
    {
        while (true)
        {
            // send player data
            SendData();

            yield return new WaitForSeconds(customDelay);
        }
    }

    void SendData()
    {
        PlayerData tmp;
        tmp.playerNum = status.playerNum;
        tmp.attackState = status.attackState;

        // position
        tmp.posX = transform.position.x;
        tmp.posY = transform.position.y;
        tmp.posZ = transform.position.z;

        // rotation
        tmp.rotX = transform.eulerAngles.x;
        tmp.rotY = transform.eulerAngles.y;
        tmp.rotZ = transform.eulerAngles.z;

        // velocity
        tmp.velX = status.rb.velocity.x;
        tmp.velY = status.rb.velocity.y;
        tmp.velZ = status.rb.velocity.z;

        // calculate the acceleration based on change in velocity and change in time (change in time calculated according to the Custom Fixed Update)
        Vector3 acceleration = (status.rb.velocity - prevVelocity) / Time.deltaTime;

        // acceleration
        tmp.accelX = acceleration.x;
        tmp.accelY = acceleration.y;
        tmp.accelZ = acceleration.z;

        // call function to have the plugin send tmp
        status.refUnityRakNet.SendMessage("SendPlayerData", tmp);

        // reset previous values
        prevVelocity = status.rb.velocity;
        prevTime = Time.time;
    }

    Vector3 DeadReckon(Vector3 p, Vector3 pPrime, Vector3 pDoublePrime)
    {
        // do dead reckoning
        float deltaTime = Time.deltaTime;

        // return p + pPrime * deltaTime + 0.5 * pDoublePrime * deltaTime^2
        return p + (pPrime * deltaTime) + (0.5f * pDoublePrime * (deltaTime * deltaTime));
    }

    public void OutOfHealth()
    {
        Debug.Log("OutOfHealth");
        // tell the plugin to notify the other players that we lost
        status.refUnityRakNet.TellLose(status.playerNum);
    }
}

public enum AttackState { STATE_NONE = 0, STATE_LUNGE_ATTACK, STATE_GROUNDPOUND_ATTACK };