/*
	EGP Networking: Plugin Test
	Dan Buckstein
	October 2018

	Developer's test application for plugin for Unity.
*/

// include the plugin interface file to take the "easy route"
// #include "../egp-net-plugin-Unity/egp-net-plugin.h"


// macro for name of plugin
#define PLUGIN_NAME "egp-net-plugin-Unity"

#ifdef _EGP_NET_PLUGIN_H_
// included plugin interface
// link static version of plugin
#pragma comment(lib,PLUGIN_NAME".lib")
#else
// no knowledge of functions in plugin
// the goal of this project is to "fake" what the plugin should do
// therefore, we need to load it and consume it like Unity will
#include <Windows.h>
#endif	// _EGP_NET_PLUGIN_H_

#include "Sprite.h"
#include <stdio.h>
//#include <string.h>

int main(int const argc, char const *const *const argv)
{
#ifndef _EGP_NET_PLUGIN_H_
	// consume library
	HMODULE library = LoadLibrary(PLUGIN_NAME".dll");
	if (library)
	{
		// load and test functions from library
		// this pointer describes a function that returns an int and takes an int as a parameter
		typedef int(*retInt_paramInt)(int);
		typedef int(*retInt_paramIntInt)(int, int);
		typedef int(*retInt_paramVoid)();
		typedef int(*retInt_paramCharStar)(char*, char*, char*);


		retInt_paramInt foo =
			(retInt_paramInt)
				GetProcAddress(library, "foo");

		retInt_paramVoid startup =
			(retInt_paramVoid)
				GetProcAddress(library, "startup");

		retInt_paramVoid shutdown =
			(retInt_paramVoid)
				GetProcAddress(library, "shutdown");

		retInt_paramCharStar initalize = (retInt_paramCharStar)
			GetProcAddress(library, "NetworkingInit");

		retInt_paramVoid netUpdate = (retInt_paramVoid)
			GetProcAddress(library, "NetworkingUpdate");

		retInt_paramIntInt sendSpriteMessage = (retInt_paramIntInt)
			GetProcAddress(library, "dispatchMessages");

		retInt_paramVoid clientInit =
			(retInt_paramVoid)
			GetProcAddress(library, "ClientInit");

		retInt_paramVoid ServerInit =
			(retInt_paramVoid)
			GetProcAddress(library, "ServerInit");

#endif	// !_EGP_NET_PLUGIN_H_

		//Sprite* mySprite = new Sprite(0, 0, 1, up);
		//Sprite* networkSprite = new Sprite(50, 50, 1, down);

		//int b = foo(4);
		//char* inputs[3];
		//inputs[0] = new char('c');
		//inputs[1] = new char('\n');
		//inputs[2] = new char('\n');
		//initalize(inputs[0], inputs[1], inputs[2]);

		const unsigned int bufferSz = 512;
		char str[bufferSz];
		// ask user for peer type
		printf("(C)lient or (S)erver?\n");
		fgets(str, bufferSz, stdin);
		//strcpy(str, clientOrServer);
		if ((str[0] == 'c') || (str[0] == 'C'))
			clientInit();
		else
			ServerInit();

		while (true)
		{
			//Checkinput
		
			/*
			Data-push
				- Server simulate
				- network update
				- display
			*/
		
			//mySprite->update();
			//networkSprite->update();
			//sendSpriteMessage(networkSprite->xPos, networkSprite->yPos);
			netUpdate();
				
		
			/*
			Data Shared
				- siumlate with latest information
				- networkUpdate
				- display
			*/
			/*
				Data Merge
				- Simulate
				- Wait for server
				- Display server simulation
			*/
		
		
		
			
		}
		

#ifndef _EGP_NET_PLUGIN_H_
		// release library
		FreeLibrary(library);
	}
#endif	// !_EGP_NET_PLUGIN_H_
}

/*
dynamic library vs static library
- static libraries are consumed, save it as part of the thing that's consuming it (compiler time linkage)
- dynamic libraries link at runtime, functions used only when they are needed
- because C++ can have many functions of the same name, it performs "name mangling" which puts a bunch of garbage around your funcion
- C doesn't allow multiple functions of the same name so there's no mangling
*/