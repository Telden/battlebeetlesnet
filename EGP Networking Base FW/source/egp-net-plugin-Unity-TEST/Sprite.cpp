#include "Sprite.h"
#include <iostream>
Sprite::Sprite(int startX, int startY, int dist, Direction dir)
{
	xPos = startX; 
	yPos = startY;
	stepDistance = dist;
	moveDirection = dir;
}

Sprite::~Sprite()
{

}

void Sprite::update()
{
	switch(moveDirection)
	{
	case up:
	{
		yPos += stepDistance;
		break;
	}
	
	case down:
	{
		yPos -= stepDistance;
		break;
	}
	case left:
	{
		xPos -= stepDistance;
		break;
	}
	case right:
	{
		xPos += stepDistance;
		break;
	}
	default:
		printf("Direction not set");
		break;
	}
	if (xPos > 700 || yPos < 0)
	{
		xPos = 0;
	}
	if (yPos > 700 || yPos < 0)
	{
		yPos = 0;
	}


	//printf("Xpos: %d  Ypos = %d\n", xPos, yPos);
}

void Sprite::serverUpdate(int xPos, int yPos)
{

}