#ifndef SPRITE_H
#define SPRITE_H
enum Direction
{
	up = 0,
	down,
	left,
	right
};

struct Sprite
{
	int xPos, yPos;
	int stepDistance;
	Direction moveDirection;

	Sprite(int startX, int startY, int dist, Direction dir);
	~Sprite();
	void update();
	void serverUpdate(int xPos, int yPos);

};


#endif // !SPRITE_H
