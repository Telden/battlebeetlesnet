/*
Matthew Roy 82-1024575
Nicholas Robbins 82-1008913
EGP-405-02, Project 3: Networked Synchronized Movement, 11/15/18

We certify that this work is entirely our own.

The assessor of this project may reproduce this project and provide copies to other academic staff,
and/or communicate a copy of this project to a plagiarism-checking service, which may retain a copy
of the project on its database.
*/

#ifndef MESSAGES_H
#define MESSAGES_H

#include "RakNet/MessageIdentifiers.h"

enum GameMessages
{
	ID_CUSTOM_MESSAGE_START = ID_USER_PACKET_ENUM,
	ID_GAME_MESSAGE_1,
};

#endif // !MESSAGES_H