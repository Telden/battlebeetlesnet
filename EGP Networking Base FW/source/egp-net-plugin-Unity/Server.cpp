/*
Matthew Roy 82-1024575
Nicholas Robbins 82-1008913
EGP-405-02, Project 3: Networked Synchronized Movement, 11/15/18

We certify that this work is entirely our own.

The assessor of this project may reproduce this project and provide copies to other academic staff,
and/or communicate a copy of this project to a plagiarism-checking service, which may retain a copy
of the project on its database.
*/

#include "Server.h"
#include "Messages.h"

Server::~Server()
{
	RakNet::RakPeerInterface::DestroyInstance(peer);
}

void Server::Initialize()
{
	// classic server initialization from old projects
	const unsigned int bufferSz = 512;
	char str[bufferSz];
	peer = RakNet::RakPeerInterface::GetInstance();
	
	printf("Enter maximum number of clients: \n");
	fgets(str, bufferSz, stdin);

	unsigned int maxClients;

	if (str[0] == '\n')
	{
		maxClients = MAX_CLIENTS_DEFAULT;
	}
	else
	{
		maxClients = atoi(str);
	}

	RakNet::SocketDescriptor sd(serverPort, 0);
	peer->Startup(maxClients, &sd, 1);

	// We need to let the server accept incoming connections from the clients
	printf("Starting the server.\n");
	peer->SetMaximumIncomingConnections(maxClients);
}