/*
	EGP Networking: Plugin Interface/Wrapper
	Dan Buckstein
	October 2018

	Main interface for plugin. Defines symbols to be exposed to whatever 
		application consumes the plugin. Targeted for Unity but should also 
		be accessible by custom executables (e.g. test app).
*/

#ifndef _EGP_NET_PLUGIN_H_
#define _EGP_NET_PLUGIN_H_

#include "egp-net-plugin-config.h"
#include "Peer.h"
#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

	EGP_NET_SYMBOL
	int NetworkingInit(char* clientOrServer, char* ip, char* maxClients);

	EGP_NET_SYMBOL
	int NetworkingUpdate();

	EGP_NET_SYMBOL
	int NetworkingStop();

	EGP_NET_SYMBOL
	int NetworkingSet(int test);

	EGP_NET_SYMBOL
	int NetworkingGet();

	EGP_NET_SYMBOL
	int dispatchMessages(int xPos, int yPos);

	EGP_NET_SYMBOL
	PlayerData getStruct();

	EGP_NET_SYMBOL
	int sendStruct(PlayerData data);

	EGP_NET_SYMBOL
	ConnectionData GetConnectionData();

	EGP_NET_SYMBOL
	int ClientInit(const char* ipAddress);

	EGP_NET_SYMBOL
	int ServerInit();

	EGP_NET_SYMBOL
	NetworkStatus GetStatus();

	EGP_NET_SYMBOL
	int ResetVariables();

	EGP_NET_SYMBOL
	int ILose(int losingID);

	EGP_NET_SYMBOL
	int CheckIfWon();

#ifdef __cplusplus
}
#endif // __cplusplus

#endif	// !_EGP_NET_PLUGIN_H_