/*
Matthew Roy 82-1024575
Nicholas Robbins 82-1008913
EGP-405-02, Project 3: Networked Synchronized Movement, 11/15/18

We certify that this work is entirely our own.

The assessor of this project may reproduce this project and provide copies to other academic staff,
and/or communicate a copy of this project to a plagiarism-checking service, which may retain a copy
of the project on its database.
*/
#ifndef PEER_H
#define PEER_H

// raknet includes
#include "RakNet/RakPeerInterface.h"
#include "RakNet/MessageIdentifiers.h"
#include "RakNet/RakNetTypes.h"
#include "RakNet/BitStream.h"

// data structures
#include <string>

#pragma pack(push, 1)
struct PlayerData
{
	// player number
	int playerNum;
	int attackState;

	// position and rotation for main gameplay
	float posX, posY, posZ;
	float rotX, rotY, rotZ;

	// velocity and acceleration for dead reckoning and correction
	float velX, velY, velZ;
	float accelX, accelY, accelZ;
};

struct NetworkStatus
{
	int newUpdates = 0;
	int newConnection = 0;
	int youWin = 0;
};

struct ConnectionData
{
	int num;
	int totalPlayers;
};

#pragma pack(pop)

class Peer
{
public:
	Peer() = default;
	~Peer() = default;

	virtual void Initialize() {};
	virtual void NetworkingReceive() {};

	// networking
	unsigned short serverPort = 60000;
	std::string IP = "127.0.0.1";
	RakNet::RakPeerInterface *peer;
	RakNet::Packet *packet;

private:

};

#endif // !PEER_H