/*
	EGP Networking: Plugin Interface/Wrapper
	Dan Buckstein
	October 2018

	Main implementation of Unity plugin wrapper.
*/

#include "egp-net-plugin.h"

// standard includes
#include <stdio.h>
#include <string.h>
#include <vector>
// RakNet includes
#include "RakNet/RakPeerInterface.h"
#include "RakNet/MessageIdentifiers.h"
#include "RakNet/RakNetTypes.h"
#include "RakNet/BitStream.h"
#include "../egp-net-plugin-Unity-TEST/Sprite.h"
#include "Peer.h"

class RaknetUnity
{
public:
	RakNet::RakPeerInterface *peer;
	RakNet::Packet *packet;

	// change this to run/stop
	int keepRunning = 1;

	// global peer settings for this app
	int isServer = 0;
	unsigned short serverPort = 60000;
	std::vector<RakNet::RakNetGUID> connectedUsers;
	RakNet::RakNetGUID serverAddress;

	int test;
};

#pragma pack(push, 1)
struct PlayerDataMessage
{
	unsigned char typeID;
	PlayerData data;
};

struct NetworkStatusMessage
{
	unsigned char typeID;
	NetworkStatus status;
};

struct ConnectionMessage
{
	unsigned char typeID;
	int num;
	int totalPlayers;
};

struct IntMessage
{
	unsigned char typeID;
	int num;
};
#pragma pack(pop)


// singleton instance of our unity version of raknet
RaknetUnity *netInstance = NULL;

// Globals because I am the best programmer in the year btw
PlayerData objData;
NetworkStatus statusData;
ConnectionData connectData;

enum GameMessages
{
	ID_CUSTOM_MESSAGE_START = ID_USER_PACKET_ENUM,
	ID_PLAYER_DATA_MESSAGE,
	ID_PLAYER_UPDATE_MESSAGE,
	ID_NETWORK_UPDATE_MESSAGE,
	ID_PLAYER_NUM_REQUEST,
	ID_CLIENT_NUM,
	ID_GAME_MESSAGE_1,
	ID_UPDATE_MESSAGE,
	ID_LOSE,
	ID_WIN,
};

struct SpriteMessage
{
	GameMessages messageType;
	int xPos, yPos;

};

//const char* DEFAULT_MAX_CLIENTS = "10";
const int DEFAULT_MAX_CLIENTS = 10;

int NetworkingInit(char* clientOrServer, char* ip, char* maxClients)
{
	NetworkingStop();

	if (netInstance == NULL)
	{
		netInstance = new RaknetUnity();
	}
	else
	{
		// no need to init again
		return 1;
	}

	// buffer
	const unsigned int bufferSz = 512;
	char str[bufferSz];
	str[0] = 'c';
	str[1] = '\n';
	// create and return instance of peer interface
	netInstance->peer = RakNet::RakPeerInterface::GetInstance();

	// global peer settings for this app
	netInstance->isServer = -1;
	netInstance->serverPort = 60000;
	netInstance->test = -1;

	// ask user for peer type
	printf("(C)lient or (S)erver?\n");
	//fgets(str, bufferSz, stdin);
	strcpy(str, clientOrServer);

	// start networking
	if ((str[0] == 'c') || (str[0] == 'C'))
	{
		RakNet::SocketDescriptor sd;
		netInstance->peer->Startup(1, &sd, 1);

		// ask user for server ip
		strcpy(str, ip);

		if (str[0] == '\n')
		{
			strcpy(str, "216.93.149.154");
			//strcpy(str, "127.0.0.1");
		}

		// start the client
		netInstance->peer->Connect(str, netInstance->serverPort, 0, 0);
		netInstance->isServer = 0;
	}
	else
	{
		// ask user for the maximum number of clients
		strcpy(str, maxClients);

		if (str[0] == '\n')
		{
			//strcpy(str, DEFAULT_MAX_CLIENTS);
		}

		unsigned int maxClients = atoi(str);
		RakNet::SocketDescriptor sd(netInstance->serverPort, 0);
		netInstance->peer->Startup(maxClients, &sd, 1);

		// we need to let the server accept incoming connections from the clients
		// starting the server
		netInstance->peer->SetMaximumIncomingConnections(maxClients);
		netInstance->isServer = 1;

		//printf("Starting Server\n");
	}
	return 0;
}

int ClientInit(const char* ipAddress)
{
	NetworkingStop();

	if (netInstance == NULL)
	{
		netInstance = new RaknetUnity();
	}
	else
	{
		// no need to init again
		return 1;
	}

	// buffer
	const unsigned int bufferSz = 512;
	char str[bufferSz];

	// create and return instance of peer interface
	netInstance->peer = RakNet::RakPeerInterface::GetInstance();

	// global peer settings for this app
	netInstance->isServer = -1;
	netInstance->serverPort = 60000;
	netInstance->test = -1;

	RakNet::SocketDescriptor sd;
	netInstance->peer->Startup(1, &sd, 1);

	//strcpy(str, "216.93.149.154");
	strcpy(str, ipAddress);

	// start the client
	netInstance->peer->Connect(str, netInstance->serverPort, 0, 0);
	netInstance->isServer = 0;
	//printf("Starting Client");

	return 0;
}

int ServerInit()
{
	//NetworkingStop();

	//if (netInstance == NULL)
	{
		netInstance = new RaknetUnity();
	}
	//else
	//{
	//	// no need to init again
	//	return 1;
	//}

	// create and return instance of peer interface
	netInstance->peer = RakNet::RakPeerInterface::GetInstance();

	// global peer settings for this app
	netInstance->isServer = -1;
	netInstance->serverPort = 60000;
	netInstance->test = -1;

	//unsigned int maxClients = atoi(DEFAULT_MAX_CLIENTS);
	RakNet::SocketDescriptor sd(netInstance->serverPort, 0);
	netInstance->peer->Startup(DEFAULT_MAX_CLIENTS, &sd, 1);

	// we need to let the server accept incoming connections from the clients
	// starting the server
	netInstance->peer->SetMaximumIncomingConnections(DEFAULT_MAX_CLIENTS);
	netInstance->isServer = 1;

	//printf("Starting Server\n");
	return 0;
}

int dispatchMessages(int xPos, int yPos)
{
	//if (netInstance->keepRunning)
	//{
	//	SpriteMessage msg;
	//	msg.messageType = ID_UPDATE_MESSAGE;
	//	msg.xPos = xPos;
	//	msg.yPos = yPos;
	//	netInstance->peer->Send((char*)&msg, sizeof(SpriteMessage), HIGH_PRIORITY, RELIABLE_ORDERED, 0, netInstance->connectedUsers[0], false);
	//}

	return 0;
}

int NetworkingUpdate()
{
	for (netInstance->packet = netInstance->peer->Receive(); netInstance->packet; netInstance->peer->DeallocatePacket(netInstance->packet), netInstance->packet = netInstance->peer->Receive())
	{
		switch (netInstance->packet->data[0])
		{
			case ID_CONNECTION_REQUEST_ACCEPTED:
			{
				//printf("Our connection request has been accepted.\n");
				// Get the server address
				netInstance->serverAddress = netInstance->packet->guid;

				break;
			}
			case ID_NEW_INCOMING_CONNECTION:
			{
				printf("A connection is incoming.\n");
				netInstance->connectedUsers.push_back(netInstance->packet->guid);
				
				ConnectionMessage numMsg;
				numMsg.typeID = ID_CLIENT_NUM;
				for (int i = 0; i < netInstance->connectedUsers.size(); i++)
				{
					if (netInstance->packet->guid == netInstance->connectedUsers[i])
					{
						numMsg.num = i + 1;
						numMsg.totalPlayers = (int)netInstance->connectedUsers.size();
					}
						
				}
				netInstance->peer->Send((char*)&numMsg, sizeof(ConnectionMessage), HIGH_PRIORITY, RELIABLE_ORDERED, 0, netInstance->packet->systemAddress, false);

				//Raise flag that a new connection has been made
				statusData.newUpdates = 1;
				statusData.newConnection = 1;
				
				//Tell the other clients that a new player has connected;
				NetworkStatusMessage newMsg;
				newMsg.typeID = ID_NETWORK_UPDATE_MESSAGE;
				newMsg.status = statusData;

				for (int i = 0; i < netInstance->connectedUsers.size(); i++)
				{
					if (netInstance->packet->guid != netInstance->connectedUsers[i])
					{
						netInstance->peer->Send((char*)&newMsg, sizeof(NetworkStatusMessage), HIGH_PRIORITY, RELIABLE_ORDERED, 0, netInstance->connectedUsers[i], false);
					}
				}
				break;
			}
			case ID_PLAYER_DATA_MESSAGE:
			{
				//printf("Recieved Player Data\t");
				
				const PlayerDataMessage* msg = (PlayerDataMessage*)netInstance->packet->data;
				objData = msg->data;
				//printf("%f\n", objData.posZ);
				//printf("%i\n", statusData.youWin);


				//Send the update to every other player
				PlayerDataMessage newMsg;
				newMsg.typeID = ID_PLAYER_DATA_MESSAGE;
				newMsg.data = objData;


				for (int i = 0; i < netInstance->connectedUsers.size(); i++)
				{
					if (netInstance->packet->guid != netInstance->connectedUsers[i])
					{
						netInstance->peer->Send((char*)&newMsg, sizeof(PlayerDataMessage), HIGH_PRIORITY, RELIABLE_ORDERED, 0, netInstance->connectedUsers[i], false);
					}
				}

				break;
			}
			case ID_LOSE:
			{
				IntMessage msg;
				msg.typeID = ID_WIN;

				for (int i = 0; i < netInstance->connectedUsers.size(); ++i)
				{
					if (netInstance->connectedUsers[i] != netInstance->packet->guid)
					{
						netInstance->peer->Send((char*)&msg, sizeof(IntMessage), HIGH_PRIORITY, RELIABLE_ORDERED, 0, netInstance->connectedUsers[i], false);
					}
				}
			}
// --------------------------------------------- CLIENT MESSAGES ---------------------------------------------//
			case ID_PLAYER_UPDATE_MESSAGE:
			{
				const PlayerDataMessage* msg = (PlayerDataMessage*)netInstance->packet->data;
				objData = msg->data;

				break;
			}
			case ID_NETWORK_UPDATE_MESSAGE:
			{
				const NetworkStatusMessage* msg = (NetworkStatusMessage*)netInstance->packet->data;
				statusData.newConnection = msg->status.newConnection;
				statusData.newUpdates = msg->status.newUpdates;
				break;
			}
			case ID_CLIENT_NUM:
			{
				const ConnectionMessage* msg = (ConnectionMessage*)netInstance->packet->data;
				connectData.num = msg->num;
				connectData.totalPlayers = msg->totalPlayers;
				break;
			}
			case ID_WIN:
			{
				statusData.youWin = 1;
				break;
			}
			default:
			{
				//printf("Message with identifier %i has arrived.\n", netInstance->packet->data[0]);
				break;
			}
		}
	}

	return 0;
}

int NetworkingStop()
{
	// shut down networking by destroying peer interface instance
	if (netInstance != NULL)
	{
		RakNet::RakPeerInterface::DestroyInstance(netInstance->peer);
		delete netInstance;
		netInstance = NULL;
		return 0;
	}

	return 1;
}

int NetworkingSet(int test)
{
	netInstance->test = test;
	return 0;
}

int NetworkingGet()
{
	return netInstance->test;
}

PlayerData getStruct()
{
	return objData;
}

int sendStruct(PlayerData data)
{
	objData = data;
	//printf("%f", data.posX);
	PlayerDataMessage msg;
	msg.typeID = ID_PLAYER_DATA_MESSAGE;
	msg.data = data;
	
	netInstance->peer->Send((char*)&msg, sizeof(PlayerDataMessage), HIGH_PRIORITY, RELIABLE_ORDERED, 0, netInstance->serverAddress, false);
	
	return 0;
}

ConnectionData GetConnectionData()
{
	//printf("Sending Client num");
	return connectData;
}

NetworkStatus GetStatus()
{
	//Make a copy of the current status to return back
	NetworkStatus tmp = statusData;

	//Lower the new changes flag in the status data
	if (statusData.newUpdates)
	{
		statusData.newUpdates = 0;
	}

	return tmp;
}

int ResetVariables()
{
	statusData.newConnection = 0;
	statusData.newUpdates = 0;
	statusData.youWin = 0;

	connectData.num = 0;
	connectData.totalPlayers = 0;

	NetworkingStop();

	return 0;
}

int ILose(int losingID)
{
	// create empty message
	IntMessage msg;
	msg.typeID = ID_LOSE;
	msg.num = losingID;

	// send to server so server can tell someone else that they won
	netInstance->peer->Send((char*)&msg, sizeof(IntMessage), HIGH_PRIORITY, RELIABLE_ORDERED, 0, netInstance->serverAddress, false);

	return 0;
}

int CheckIfWon()
{
	return statusData.youWin;
}